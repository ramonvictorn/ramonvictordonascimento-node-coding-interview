import { NotFoundError } from 'routing-controllers';
import { FlightsModel } from '../models/flights.model'

export class FlightsService {
    getAll() {
        return FlightsModel.find()
    }

    async addPassager(flightId: string, passagerId: string) {
        const flight = await FlightsModel.findById(flightId);
        if (!flight) {
            throw new NotFoundError(`FLIGHT_NOT_FOUND`); 
        }

        const updatedFlight = await FlightsModel.findByIdAndUpdate({
            _id: flightId,
        }, {
            passengers: [...flight.passengers, passagerId]
        }, {
            returnDocument: 'after',
        });
    
        return {
            flight: updatedFlight,
        }
    }
}
