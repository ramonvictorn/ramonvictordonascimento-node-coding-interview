import { JsonController, Get, Post, Param } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Post('/:flightId/add-passager/:passagerId', { transformResponse: false })
    async addPassager(@Param('flightId') flightId: string, @Param('passagerId') passagerId: string) {
        return {
            status: 200,
            data: await flightsService.addPassager(flightId, passagerId),
        }
    }
}
